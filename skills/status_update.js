[
    {
        "command": "status_update",
        "description": "Asks user for status, collects data.",
        "modified": "2018-11-13T15:10:03.974Z",
        "__v": 0,
        "created": "2018-11-12T21:30:27.414Z",
        "script": [
            {
                "topic": "default",
                "script": [
                    {
                        "text": [
                            "Hey! It's time to submit your daily status update! <br>\n<i>(Click \"Ready!\" to begin, or \"No update\" to skip today's update.)</i>"
                        ],
                        "platforms": {
                            "teams": {
                                "attachments": [
                                    {
                                        "type": "hero",
                                        "title": "Ready to start?",
                                        "name": "New attachment",
                                        "buttons": [
                                            {
                                                "type": "imBack",
                                                "title": "Ready!",
                                                "value": "Ready!"
                                            },
                                            {
                                                "type": "imBack",
                                                "title": "No update",
                                                "value": "No update"
                                            }
                                        ],
                                        "images": []
                                    }
                                ]
                            }
                        },
                        "collect": {
                            "key": "chose_update",
                            "options": [
                                {
                                    "default": true,
                                    "pattern": "default",
                                    "action": "next"
                                },
                                {
                                    "pattern": "Ready!",
                                    "type": "string",
                                    "action": "update"
                                },
                                {
                                    "pattern": "No update",
                                    "type": "string",
                                    "action": "no_update"
                                }
                            ]
                        }
                    },
                    {
                        "text": [
                            "Sorry, I don't understand... Could you select one of the options shown in the message?"
                        ],
                        "collect": {
                            "key": "chose_update",
                            "options": [
                                {
                                    "default": true,
                                    "pattern": "default",
                                    "action": "repeat"
                                },
                                {
                                    "pattern": "Ready!",
                                    "type": "string",
                                    "action": "update"
                                },
                                {
                                    "pattern": "No update",
                                    "type": "string",
                                    "action": "no_update"
                                }
                            ]
                        },
                        "platforms": {
                            "teams": {
                                "attachments": [
                                    {
                                        "type": "hero",
                                        "title": "Pick one of these:",
                                        "name": "New attachment",
                                        "buttons": [
                                            {
                                                "type": "imBack",
                                                "title": "Ready!",
                                                "value": "Ready!"
                                            },
                                            {
                                                "type": "imBack",
                                                "title": "No update",
                                                "value": "No update"
                                            }
                                        ],
                                        "images": []
                                    }
                                ]
                            }
                        }
                    },
                    {
                        "action": "complete"
                    }
                ]
            },
            {
                "topic": "on_timeout",
                "script": [
                    {
                        "text": [
                            "Looks like you're busy. No worries! You can always say \"Start update\" to start a new update."
                        ]
                    },
                    {
                        "action": "timeout"
                    }
                ]
            },
            {
                "topic": "update",
                "script": [
                    {
                        "text": [
                            "Okay! Let's get started."
                        ],
                        "meta": [],
                        "collect": null
                    },
                    {
                        "text": [
                            "<b>What did you do yesterday?</b>"
                        ],
                        "collect": {
                            "key": "user_did",
                            "options": [
                                {
                                    "default": true,
                                    "pattern": "default",
                                    "action": "next"
                                }
                            ]
                        }
                    },
                    {
                        "text": [
                            "Noice. Next question!",
                            "Sweet! Next up...",
                            "Okie dokie, then...",
                            "Awesome! Okay, then..."
                        ]
                    },
                    {
                        "text": [
                            "<b>What are you planning to do today?</b>"
                        ],
                        "collect": {
                            "key": "user_doing",
                            "options": [
                                {
                                    "default": true,
                                    "pattern": "default",
                                    "action": "next"
                                }
                            ]
                        }
                    },
                    {
                        "text": [
                            "Sounds good, last question...",
                            "Productive! I like it.",
                            "Good plan! Last one...",
                            "Perfect! Okay, last one..."
                        ]
                    },
                    {
                        "text": [
                            "<b>Do you have any blockers?<b>"
                        ],
                        "collect": {
                            "key": "user_blockers",
                            "options": [
                                {
                                    "default": true,
                                    "pattern": "default",
                                    "action": "next"
                                }
                            ]
                        }
                    },
                    {
                        "text": [
                            "Okay, that's everything I need for today. Talk to you again soon!"
                        ]
                    },
                    {
                        "action": "complete"
                    }
                ]
            },
            {
                "topic": "no_update",
                "script": [
                    {
                        "text": [
                            "Dang, I wanted to spend time with you... 😢",
                            "Aw, darn... No update?",
                            "Oh, okay..."
                        ]
                    },
                    {
                        "text": [
                            "Could you let me know why?"
                        ],
                        "collect": {
                            "key": "user_reason",
                            "options": [
                                {
                                    "default": true,
                                    "pattern": "default",
                                    "action": "next"
                                }
                            ]
                        }
                    },
                    {
                        "text": [
                            "Phew! I thought you just didn't like me for a minute there...",
                            "Thanks! I got it.",
                            "Okay, sounds good."
                        ]
                    },
                    {
                        "text": [
                            "Okay, that's everything I need for today. Talk to you again soon!"
                        ]
                    },
                    {
                        "action": "stop"
                    }
                ]
            },
            {
                "script": [
                    {
                        "text": [
                            "This is undefined"
                        ]
                    },
                    {
                        "action": "complete"
                    }
                ]
            }
        ],
        "variables": [
            {
                "name": "user_reason",
                "type": "string",
                "id": 292
            },
            {
                "name": "user_blockers",
                "type": "string",
                "id": 141
            },
            {
                "name": "user_doing",
                "type": "string",
                "id": 141
            },
            {
                "name": "user_did",
                "type": "string",
                "id": 73
            },
            {
                "name": "chose_update",
                "type": "string",
                "id": 330
            },
            {
                "name": "question_1",
                "type": "string"
            },
            {
                "name": "question_2",
                "type": "string"
            },
            {
                "name": "question_3",
                "type": "string"
            }
        ],
        "triggers": [
            {
                "pattern": "Start update",
                "type": "string",
                "id": 141
            },
            {
                "type": "string",
                "pattern": "status_update"
            }
        ],
        "tags": []
    }
]